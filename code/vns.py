import random
import time
from graphProblem import *
from solution import *
import networkx as nx

#+++++++++++++++++++++++++++++++++++++++++Coder une metaheuristique de type Variable Neighborhood Search (VNS).++++
class VNS:
    def __init__(self, n, max_evaluations):
    # Initialisation de l'objet ILS avec le nombre de noeuds et le nombre maximal d'évaluations
        self.n = n  # Nombre de noeuds
        self.max_evaluations = max_evaluations  # Nombre maximum d'évaluations permises

    def random_solution(self):
        """
        Crée une solution aléatoire avec n noeuds
        """
        solution = Solution(self.n)  # Initialisation d'une nouvelle solution
        for i in range(self.n):
            for j in range(i + 1, self.n):
                if random.random() < 0.5:  # Choix aléatoire de la présence ou non d'une arête entre chaque paire de noeuds
                    solution.add_edge(i, j)  # Ajout de l'arête à la solution si le choix est positif
        return solution

    def elementary_neighborhood(self, solution):
        """
        Crée un voisinage élémentaire d'une solution en permutant deux arêtes aléatoires
        """
        new_solution = solution.copy(Solution(solution.n))  # Initialisation d'une nouvelle solution qui est une copie de la solution donnée
        i, j = random.sample(range(solution.n), 2)  # Sélection de deux noeuds au hasard
        new_solution.flip_edge(i, j)  # Inversion de l'état de l'arête entre les deux noeuds sélectionnés
        return new_solution


#++++++++++++++++++++++++Proposer des voisinages plus larges que le voisinage elementaire precedant++++++++++++++++
    def k_opt(self, solution, k):
        """
        Crée un voisinage k-opt d'une solution
        """
        new_solution = solution.copy(Solution(solution.n))

        removed_edges = random.sample(list(new_solution.G.edges), k)

        for edge in removed_edges:
            new_solution.remove_edge(*edge)
        nodes = list(range(new_solution.n))
        random.shuffle(nodes)
        for i in range(k):
            new_solution.add_edge(nodes[i], nodes[(i + 1) % k])
        return new_solution

    def run(self):
        problem = GraphProblem(self.n)
        current_solution = self.random_solution()
        problem.eval(current_solution)
        num_evaluations = 1
        num_improvements = 0
        start_time = time.time()

        # Définition des structures de voisinage
        neighborhood_structures = [lambda x: self.elementary_neighborhood(x),
                                   lambda x: self.k_opt(x, 2),
                                   lambda x: self.k_opt(x, 3),
                                   lambda x: self.k_opt(x, 4)]

        neighborhood_index = 0
        while num_evaluations < self.max_evaluations:
            neighborhood = neighborhood_structures[neighborhood_index]
            new_solution = neighborhood(current_solution)
            problem.eval(new_solution)
            num_evaluations += 1

            if new_solution.fitness > current_solution.fitness:
                current_solution = new_solution
                num_improvements += 1
                neighborhood_index = 0
            else:
                neighborhood_index += 1

            if neighborhood_index >= len(neighborhood_structures):
                neighborhood_index = 0

        end_time = time.time()

        print("Solution finale : ", current_solution)
        print("Nombre d'évaluations : ", num_evaluations)
        print("Nombre d'améliorations : ", num_improvements)
        print("Temps d'exécution : ", end_time - start_time)

        return current_solution



if __name__ == '__main__':
    vns = VNS(n=18, max_evaluations=100)
    solution = vns.run()

    # Enregistrement de la solution dans un fichier png
    solution.draw("vns.png")

    # Affichage de la matrice d'adjacence de la solution
    print(nx.adjacency_matrix(solution.G).todense())
