import networkx as nx
import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Dense


def graph2vec(g):
    """Takes a nx.Graph object, and returns a vector corresponding to the lower
    half of the adjacency matrix of the graph, diagonal not included."""
    size = g.number_of_nodes()
    v = [0] * (size*(size-1)//2)
    for (i,j) in g.edges:
        if i < j:
            s = i
            i = j
            j = s
        v[i*(i-1)//2+j] = 1
    return v


def vec2graph(v):
    """Takes a vector and returns the corresponding graph."""
    size = (1 + np.sqrt(1 + 8*len(v))) / 2
    assert size == int(size)
    size = int(size)
    g = nx.Graph()
    g.add_node(0)
    cpt = 0
    for i in range(1,size):
        g.add_node(i)
        for j in range(i):
            if v[cpt] == 1:
                g.add_edge(i,j)
            cpt += 1
    return g


def f(v):
    """Evaluates a vector representing a graph. If f(v) is negative, then the
    proposition is proven false. If the graph is not connected, then infinity
    is returned."""
    g = vec2graph(v)
    if not nx.is_connected(g):
        return np.infty
    mu = len(nx.max_weight_matching(g))
    e1 = max(nx.adjacency_spectrum(g).real)
    size = g.number_of_nodes()
    return e1 + mu - (size-1)**0.5 + 1


def Uvgraph(g_size):
    """Generates a random undirected graph of size g_size uniformly, in vector form."""
    return np.random.randint(0, 2, g_size*(g_size-1)//2)


def weights2vec(ws):
    return np.concatenate([a.numpy().flatten() for a in ws])


def vec2weights(v, lshapes):
    start = 0
    ws = []
    for shape in lshapes:
        w = v[start:start+np.prod(shape)].reshape(shape)
        ws.append(w)
        start += np.prod(shape)
    return ws


class PGraph:

    def __init__(self, n):
        self.vsize = int(n*(n-1)/2)
        self.nn = tf.keras.models.Sequential()
        self.nn.add(Dense(128, input_shape=(2*self.vsize,), activation="sigmoid"))
        self.nn.add(Dense(64, activation="sigmoid"))
        self.nn.add(Dense(4, activation="sigmoid"))
        self.nn.add(Dense(1, activation="sigmoid"))
        self.lshapes = [a.shape for a in self.nn.trainable_variables]


    def generate_graphs(self, nb_graphs):
        vec = np.zeros((nb_graphs, 2*self.vsize))
        for i in range(self.vsize):
            vec[:,self.vsize+i] = 1
            p = self.nn(vec)
            for j in range(nb_graphs):
                if np.random.rand() < p[j]:
                    vec[j,i] = 1
            vec[:, self.vsize+i] = 0
        return vec[:, :self.vsize]


    def get_vweights(self):
        return weights2vec(self.nn.trainable_variables)


    def set_vweights(self, v):
        self.nn.set_weights(vec2weights(v, self.lshapes))

