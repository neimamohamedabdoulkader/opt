import tensorflow as tf
from tensorflow.keras.optimizers import Adam

class GradientDescent:

    def __init__(self, problem, solution):
        self.problem = problem
        self.solution = solution
        self.optimizer = Adam(lr=0.01)

    def optimize(self, num_iterations):
        for i in range(num_iterations):
            with tf.GradientTape() as tape:
                fitness = self.problem.eval(self.solution)
                loss = tf.math.negative(fitness)
            gradients = tape.gradient(loss, self.solution.nn.trainable_variables)
            self.optimizer.apply_gradients(zip(gradients, self.solution.nn.trainable_variables))
        return self.solution.nn
