import time
import numpy as np
import networkx as nx

from ils import ILS
from vns import VNS

# Instancier des objets ILS et VNS
ils = ILS(10, 100)
vns = VNS(10, 100)

# Exécuter ILS plusieurs fois et calculer la fitness moyenne et le temps d'exécution moyen
n_runs = 10
fitness_ils = []
time_ils = []
for i in range(n_runs):
    start_time = time.time()
    solution_ils = ils.ils()
    time_ils.append(time.time() - start_time)
    fitness_ils.append(solution_ils.fitness)
avg_fitness_ils = np.mean(fitness_ils)
std_fitness_ils = np.std(fitness_ils)
avg_time_ils = np.mean(time_ils)
std_time_ils = np.std(time_ils)

# Exécuter VNS plusieurs fois et calculer la fitness moyenne et le temps d'exécution moyen
fitness_vns = []
time_vns = []
for i in range(n_runs):
    start_time = time.time()
    solution_vns = vns.run()
    time_vns.append(time.time() - start_time)
    fitness_vns.append(solution_vns.fitness)
avg_fitness_vns = np.mean(fitness_vns)
std_fitness_vns = np.std(fitness_vns)
avg_time_vns = np.mean(time_vns)
std_time_vns = np.std(time_vns)

# Afficher les résultats
print("Fitness moyenne d'ILS : {:.2f} +/- {:.2f}".format(avg_fitness_ils, std_fitness_ils))
print("Temps d'exécution moyen d'ILS : {:.2f} +/- {:.2f}".format(avg_time_ils, std_time_ils))
print("Fitness moyenne de VNS : {:.2f} +/- {:.2f}".format(avg_fitness_vns, std_fitness_vns))
print("Temps d'exécution moyen de VNS : {:.2f} +/- {:.2f}".format(avg_time_vns, std_time_vns))
