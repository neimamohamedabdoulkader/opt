import numpy as np
from solutionNN import *
from solution import *


"""
    Evaluation function based on the neural network generator
    Generate only 1 graph (deterministic) given by the probabilities computed by a NN
"""
class NN_GraphProblem:
    def __init__(self, problem):
        self.problem = problem

    def eval(self, solution):
        g = np.zeros((1, 2 * solution.vsize))

        for i in range(solution.vsize):
            g[0, solution.vsize + i] = 1
            p = solution.nn(g)

            if p[j] < 0.5:
                g[0, i] = 0
            else:
                g[0, i] = 1

            g[0, solution.vsize + i] = 0

        # fitness value
        for i in range(solution.vsize):
            solution.set_bit(i, g[0, i] == 1)

        self.problem.eval(solution)

"""
    Evaluation function based on the neural network generator
    Generate only k graphs based on the probabilities given by the NN, 
    and then the fitness value is computed by the average of quality of each graph
"""
class StochasticNN_GraphProblem:
    def __init__(self, problem, sampleSize):
        self.problem = problem
        self.sampleSize = sampleSize

    def eval(self, solution):
        g = np.zeros((self.sampleSize, 2 * solution.vsize))

        for i in range(solution.vsize):
            g[:, solution.vsize + i] = 1
            p = solution.nn(g)

            for j in range(self.sampleSize):
                if np.random.rand() < p[j]:
                    g[j, i] = 1

            g[:, solution.vsize + i] = 0

        # Average value of fitness of the sample size
        f = 0
        for j in range(self.sampleSize):
            for i in range(solution.vsize):
                solution.set_bit(i, g[j, i] == 1)
            self.problem.eval(solution)
            f += solution.fitness()

        solution.fitness = f / solution.vsize


"""
    Generator a set of graph solutions 
    defined by the probabilities of the NN
"""
class NeuralNetwork_GraphGenerator:

    def __init__(self, n, nb_graphs):
        self.vsize = int(n*(n-1)/2)
        self.nn = tf.keras.models.Sequential()
        self.nn.add(Dense(128, input_shape=(2*self.vsize,), activation="sigmoid"))
        self.nn.add(Dense(64, activation="sigmoid"))
        self.nn.add(Dense(4, activation="sigmoid"))
        self.nn.add(Dense(1, activation="sigmoid"))
        self.lshapes = [a.shape for a in self.nn.trainable_variables]
        self.nb_graphs = nb_graphs

    def generate_graphs(self, solution):
        solutions = [ ]

        g = np.zeros((self.nb_graphs, 2*self.vsize))

        for i in range(self.vsize):
            g[:, self.vsize + i] = 1
            p = solution.nn(vec)
            for j in range(self.nb_graphs):
                if np.random.rand() < p[j]:
                    g[j, i] = 1
            g[:, self.vsize + i] = 0

        for j in range(self.nb_graphs):
            s = Solution(solution.n)
            for i in range(solution.vsize):
                s.set_bit(i, g[j, i] == 1)
            solutions.append(s)

        return solutions

