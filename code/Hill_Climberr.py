# Importation des modules nécessaires
from solution import *
from graphProblem import *
import random
import time
import csv

class HillClimber:
    def __init__(self, n, max_evaluations):
        # Initialisation de l'objet HillClimber avec le nombre de noeuds et le nombre maximal d'évaluations
        self.problem = GraphProblem(n)  # Initialisation du problème en créant une instance de GraphProblem avec n noeuds
        self.n = n  # Nombre de noeuds
        self.max_evaluations = max_evaluations  # Nombre maximum d'évaluations permises

# Crée une solution aléatoire avec n noeuds
    def random_solution(self):
        
        solution = Solution(self.n)  # Initialisation d'une nouvelle solution
        for i in range(self.n):
            for j in range(i + 1, self.n):
                if random.random() < 0.5:  # Choix aléatoire de la présence ou non d'une arête entre chaque paire de noeuds
                    solution.add_edge(i, j)  # Ajout de l'arête à la solution si le choix est positif
        return solution

#Crée un voisinage élémentaire d'une solution en permutant deux arêtes aléatoires
      
    def elementary_neighborhood(self, solution):
    
        new_solution = solution.copy(Solution(solution.n))  # Initialisation d'une nouvelle solution qui est une copie de la solution donnée
        i, j = random.sample(range(solution.n), 2)  # Sélection de deux noeuds au hasard
        new_solution.flip_edge(i, j)  # Inversion de l'état de l'arête entre les deux noeuds sélectionnés
        return new_solution
#    Exécute l'algorithme Hill Climber pour optimiser le problème donné
    def hill_climber(self):
        current_solution = self.random_solution()  # Initialisation de la solution courante avec une solution aléatoire
        self.problem.eval(current_solution)  # Évaluation de la solution courante
        num_evaluations = 1  # Initialisation du nombre d'évaluations effectuées à 1
        num_improvements = 0  # Initialisation du nombre d'améliorations effectuées à 0
        start_time = time.time()  # Enregistrement du temps de départ de l'algorithme
        evaluations_list = []  # Liste pour stocker le nombre d'évaluations effectuées à chaque itération
        improvements_list = []  # Liste pour stocker le nombre d'améliorations effectuées à chaque itération
        time_list = []  # Liste pour stocker le temps écoulé à chaque itération
        fitness_list = []  # Liste pour stocker la fitness de la solution courante à chaque itération

        # Ajout des métriques de la première itération aux listes
        evaluations_list.append(num_evaluations)
        improvements_list.append(num_improvements)
        time_list.append(0)
        fitness_list.append(current_solution.fitness)

        while num_evaluations < self.max_evaluations:
            neighborhood = [self.elementary_neighborhood(current_solution)]  # Création d'un voisinage élémentaire de la solution courante en permutant deux arêtes aléatoires
            best_neighbor = None  # Initialisation du meilleur voisin à None
            best_score = current_solution.fitness  # Initialisation du meilleur score avec la fitness de la solution courante


            for neighbor in neighborhood:
                self.problem.eval(neighbor)
                num_evaluations += 1
                if neighbor.fitness > best_score:
                    best_neighbor = neighbor
                    best_score = neighbor.fitness
                    num_improvements += 1

            if best_score > current_solution.fitness:
                current_solution = best_neighbor

           
            evaluations_list.append(num_evaluations)
            improvements_list.append(num_improvements)
            time_list.append(time.time() - start_time)
            fitness_list.append(current_solution.fitness)

        end_time = time.time()

        print("Solution finale: ", current_solution)
        print("Nombre d'évaluations: ", num_evaluations)
        print("Nombre d'améliorations: ", num_improvements)
        print("Temps d'exécution: ", end_time - start_time)

        # write to CSV file
        with open('hill_climber_performance.csv', mode='w', newline='') as file:
             writer = csv.writer(file)
             writer.writerow(['Num. Evaluations', 'Num. Improvements', 'Execution Time', 'Fitness'])
             for i in range(len(evaluations_list)):
                 writer.writerow([evaluations_list[i], improvements_list[i], time_list[i], fitness_list[i]])

        return current_solution


if __name__ == '__main__':
    hill= HillClimber(19, 100)
        # Exécute l'algorithme Hill Climber et enregistre la solution dans un fichier png
    solution = hill.hill_climber()
    solution.draw("hill_climber.png")

        # Matrice d'adjacence
    print(nx.adjacency_matrix(solution.G).todense())

    