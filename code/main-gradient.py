import numpy as np
import tensorflow as tf
from solution import Solution
from solutionNN import SolutionNN
from nn_graphGenerator import NN_GraphProblem, NeuralNetwork_GraphGenerator

def main():
    # define problem parameters
    n = 6
    nb_graphs = 100
    sample_size = 5

    # create initial solution with neural network
    initial_solution = SolutionNN(n)
    graph_generator = NeuralNetwork_GraphGenerator(n, nb_graphs)
    graph_problem = NN_GraphProblem(initial_solution)

    # define optimization parameters
    max_iterations = 1000
    tolerance = 1e-4
    learning_rate = 0.01

    # run optimization algorithm
    current_solution = initial_solution
    current_fitness = graph_problem.eval(current_solution).fitness()
    best_solution = current_solution
    best_fitness = current_fitness

    for iteration in range(max_iterations):
        # generate new solutions and evaluate them
        new_solutions = graph_generator.generate_graphs(current_solution)
        for new_solution in new_solutions:
            new_fitness = graph_problem.eval(new_solution).fitness()

            # update best solution
            if new_fitness < best_fitness:
                best_solution = new_solution
                best_fitness = new_fitness

            # update current solution
            if new_fitness < current_fitness:
                current_solution = new_solution
                current_fitness = new_fitness

        # check for convergence
        if best_fitness < tolerance:
            break

        # update neural network weights
        for i in range(len(current_solution.nn.trainable_variables)):
            gradient = np.zeros(current_solution.lshapes[i])
            for new_solution in new_solutions:
                x = np.concatenate((current_solution.get_binary(), new_solution.get_binary()))
                p1 = current_solution.nn.predict(x)[0]
                p2 = new_solution.nn.predict(x)[1]
                gradient += (p1 - p2) * x
            current_solution.nn.trainable_variables[i].assign_sub(learning_rate * gradient)

        # print iteration progress
        print("Iteration {}: best_fitness={}".format(iteration, best_fitness))

    # print final result
    print("Optimization finished: best_fitness={}".format(best_fitness))

if __name__ == "__main__":
    main()
