# Importation des modules nécessaires
import random
import time
import networkx as nx
from graphProblem import GraphProblem
from solution import Solution
import csv

class ILS:
    def __init__(self, n, max_evaluations):
         # Initialisation de l'objet ILS avec le nombre de noeuds et le nombre maximal d'évaluations
        self.problem = GraphProblem(n)  # Initialisation du problème en créant une instance de GraphProblem avec n noeuds
        self.n = n  # Nombre de noeuds
        self.max_evaluations = max_evaluations  # Nombre maximum d'évaluations permises

    def random_solution(self):
        """
        Crée une solution aléatoire avec n noeuds
        """
        solution = Solution(self.n)  # Initialisation d'une nouvelle solution
        for i in range(self.n):
            for j in range(i + 1, self.n):
                if random.random() < 0.5:  # Choix aléatoire de la présence ou non d'une arête entre chaque paire de noeuds
                    solution.add_edge(i, j)  # Ajout de l'arête à la solution si le choix est positif
        return solution

    def elementary_neighborhood(self, solution):
        """
        Crée un voisinage élémentaire d'une solution en permutant deux arêtes aléatoires
        """
        new_solution = solution.copy(Solution(solution.n))  # Initialisation d'une nouvelle solution qui est une copie de la solution donnée
        i, j = random.sample(range(solution.n), 2)  # Sélection de deux noeuds au hasard
        new_solution.flip_edge(i, j)  # Inversion de l'état de l'arête entre les deux noeuds sélectionnés
        return new_solution

    def disruptive_neighborhood(self, solution):
        """
        Crée un voisinage perturbateur d'une solution en inversant deux arêtes aléatoires
        """
        new_solution = solution.copy(Solution(solution.n))
        i, j = random.sample(range(solution.n), 2)
        new_solution.flip_edge(i, j)
        k, l = random.sample([x for x in range(solution.n) if x not in [i, j]], 2)
        new_solution.flip_edge(k, l)
        return new_solution

    def ils(self):
        """
        Exécute la métaheuristique Iterated Local Search (ILS) avec un opérateur perturbateur pour optimiser le problème donné
        """
        current_solution = self.random_solution()
        problem = GraphProblem(self.n)
        problem.eval(current_solution)
        num_evaluations = 1
        num_improvements = 0
        start_time = time.time()

        evaluations_list = []  # Liste pour stocker le nombre d'évaluations effectuées à chaque itération
        improvements_list = []  # Liste pour stocker le nombre d'améliorations effectuées à chaque itération
        time_list = []  # Liste pour stocker le temps écoulé à chaque itération
        fitness_list = []  # Liste pour stocker la fitness de la solution courante à chaque itération

        # Ajout des métriques de la première itération aux listes
        evaluations_list.append(num_evaluations)
        improvements_list.append(num_improvements)
        time_list.append(0)
        fitness_list.append(current_solution.fitness)

        while num_evaluations < self.max_evaluations:
            best_solution = current_solution.copy(Solution(current_solution.n))
            for i in range(10):
                neighborhood = [self.elementary_neighborhood(current_solution), self.disruptive_neighborhood(current_solution)]
                for neighbor in neighborhood:
                    problem.eval(neighbor)
                    num_evaluations += 1
                    if neighbor.fitness > best_solution.fitness:
                        best_solution = neighbor.copy(Solution(neighbor.n))
                        num_improvements += 1
                current_solution = best_solution.copy(Solution(best_solution.n))

                evaluations_list.append(num_evaluations)
                improvements_list.append(num_improvements)
                time_list.append(time.time() - start_time)
                fitness_list.append(current_solution.fitness)

        end_time = time.time()

        print("Solution finale : ", current_solution)
        print("Nombre d'évaluations : ", num_evaluations)
        print("Nombre d'améliorations : ", num_improvements)
        print("Temps d'exécution : ", end_time - start_time)

            # write to CSV file
        with open('ils.csv', mode='w', newline='') as file:
             writer = csv.writer(file)
             writer.writerow(['Num. Evaluations', 'Num. Improvements', 'Execution Time', 'Fitness'])
             for i in range(len(evaluations_list)):
                 writer.writerow([evaluations_list[i], improvements_list[i], time_list[i], fitness_list[i]])
        return current_solution

if __name__ == '__main__':
    ils = ILS(18, 100)
    solution = ils.ils()

    print(solution)

    # Sauvegarder en fichier png
    solution.draw("ils.png")

    # Matrice d'adjacence
    print(nx.adjacency_matrix(solution.G).todense())
