import tensorflow as tf
from tensorflow.keras.layers import Dense
from solution import *

class SolutionNN(Solution):
	def __init__(self, n):
                
		# number of edges
                self.vsize = int(self.n * (self.n - 1)/2)

                self.nn = tf.keras.models.Sequential()
                self.nn.add(Dense(128, input_shape=(2*self.vsize,), activation="sigmoid"))
                self.nn.add(Dense(64, activation="sigmoid"))
                self.nn.add(Dense(4, activation="sigmoid"))
                self.nn.add(Dense(1, activation="sigmoid"))
                self.lshapes = [a.shape for a in self.nn.trainable_variables]

